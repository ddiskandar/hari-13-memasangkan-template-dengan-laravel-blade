@extends('layout.master')

@section('title', 'Dede Iskandar')

@section('content')
<div class="container-fluid">
    <div class="card">
        <div class="card-body">
          <h5 class="card-title">Card title</h5>

          <p class="card-text">
            Some quick example text to build on the card title and make up the bulk of the card's
            content.
          </p>

          <a href="{{ route('table') }}" class="card-link">Table</a>
          <a href="{{ route('data-tables') }}" class="card-link">Data-Tables</a>
        </div>
      </div>
  </div><!-- /.container-fluid -->

@endsection
